package com.equipo3.ejercicio.repository;

import com.equipo3.ejercicio.controllers.TarjetasController;
import com.equipo3.ejercicio.model.tarjetas;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TarjetaRepositoryImplement implements TarjetaRepository {
    private final MongoOperations mongoOperations;

    @Autowired
    public TarjetaRepositoryImplement(MongoOperations mongoOperations) {
        this.mongoOperations = mongoOperations;
    }

    @Override
    public List<tarjetas> findAll() {
        List<tarjetas> nTarjetas = this.mongoOperations.find(new Query(), tarjetas.class);
        return nTarjetas;
    }

    @Override
    public tarjetas findOne(String id) {
        tarjetas encontrado = this.mongoOperations.findOne(new Query(Criteria.where("nTarjeta").is(id)),tarjetas.class);
        return encontrado;
    }

    @Override
    public tarjetas saveTarjeta(tarjetas tar) {
        this.mongoOperations.save(tar);
        return findOne(tar.getnTarjeta());
    }

    @Override
    public void updateTarjeta(tarjetas tar) {
        this.mongoOperations.save(tar);
    }

    @Override
    public void deleteTarjeta(String id) {
        this.mongoOperations.findAndRemove(new Query(Criteria.where("id").is(id)), tarjetas.class);
    }



}
