package com.equipo3.ejercicio.model;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

@Document(collection = "tarjetas")
@JsonPropertyOrder({"nTarjeta", "bandera", "tipoTarjeta"})
public class tarjetas implements Serializable {
    @Id
    @NotNull
    private String nTarjeta;
    @NotNull
    private String bandera;
    private String tipoTarjeta;

    public tarjetas(String nTarjeta, String bandera, String tipoTarjeta)
    {
        this.setnTarjeta(nTarjeta);
        this.setBandera(bandera);
        this.setTipoTarjeta(tipoTarjeta);
    }

    public String getnTarjeta() {
        return nTarjeta;
    }

    public void setnTarjeta(String nTarjeta) {
        this.nTarjeta = nTarjeta;
    }

    public String getBandera() {
        return bandera;
    }

    public void setBandera(String bandera) {
        this.bandera = bandera;
    }

    public String getTipoTarjeta() {
        return tipoTarjeta;
    }

    public void setTipoTarjeta(String tipoTarjeta) {
        this.tipoTarjeta = tipoTarjeta;
    }
}
