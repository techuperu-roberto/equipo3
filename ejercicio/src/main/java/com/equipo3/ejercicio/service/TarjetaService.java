package com.equipo3.ejercicio.service;

import com.equipo3.ejercicio.model.tarjetas;

import java.util.List;

public interface TarjetaService {
    List<tarjetas> findAll();
    public tarjetas findOne(String id);
    public tarjetas saveTarjeta(tarjetas tar);
    public void updateTarjeta(tarjetas tar);
    public void deleteTarjeta(String id);
}
