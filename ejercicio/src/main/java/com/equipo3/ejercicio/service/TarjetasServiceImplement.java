package com.equipo3.ejercicio.service;

import com.equipo3.ejercicio.model.tarjetas;
import com.equipo3.ejercicio.repository.TarjetaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("TarjetaService")
@Transactional
public class TarjetasServiceImplement implements TarjetaService {
    private TarjetaRepository tarjetaRepository;
    @Autowired
    public TarjetasServiceImplement(TarjetaRepository tarjetaRepository){
        this.tarjetaRepository =tarjetaRepository;
    }
    @Override
    public List<tarjetas> findAll(){
        return tarjetaRepository.findAll();
    }
    @Override
    public tarjetas findOne(String id) {
        return tarjetaRepository.findOne(id);
    }

    @Override
    public tarjetas saveTarjeta(tarjetas tar) {
        return tarjetaRepository.saveTarjeta(tar);
    }

    @Override
    public void updateTarjeta(tarjetas tar) {
        tarjetaRepository.updateTarjeta(tar);
    }

    @Override
    public void deleteTarjeta(String id) {
        tarjetaRepository.deleteTarjeta(id);
    }
}
