package com.equipo3.ejercicio.controllers;

import com.equipo3.ejercicio.model.tarjetas;
import com.equipo3.ejercicio.service.TarjetaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("tarjetas")
public class TarjetasController {
    private final TarjetaService tarjetaService;
    private tarjetas tarjetas;

    @Autowired
    public TarjetasController(TarjetaService tarjetaService){
        this.tarjetaService = tarjetaService;
    }
    @GetMapping()
    public ResponseEntity<List<tarjetas>> tarjetas(){
        System.out.print("Lista de tarjetas");
        return ResponseEntity.ok(tarjetaService.findAll());
    }

    @PostMapping
    public ResponseEntity<tarjetas> saveTarjeta(@RequestBody tarjetas tarjeta){
        return ResponseEntity.ok(tarjetaService.saveTarjeta(tarjeta));
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<tarjetas> findOne(@PathVariable String id){
        return ResponseEntity.ok(tarjetaService.findOne(id));
    }
}
